let alunos = require('./alunos.js') // aqui importamos nosso arquivo alunos.js para a variável "alunos"

/* com nossas alunos disponíveis em uma lista chamada "alunos" é hora de percorrermos a lista
calculando as médias:
*/

alunos.forEach((aluno, index) => {
  /* Aqui, cada "aluno" é um item da nossa lista de "alunos" e nós podemos
  acessar o valor de cada propriedade do nosso pela chave
  (no caso, temos as chaves nome e notas)
  */
  let nome = aluno.nome
  let listaDeNotas = aluno.notas

  /* Observação: para calcular a médias precisaremos saber o total das notas e
    dividir isso pela quantidade de notas, para isso, descobriremos QUANTAS NOTAS
    temos utilizando a propriedade "length" da nossa lista
  */

  let quantidadeNotas = listaDeNotas.length

  /* ATENÇÃO ESPECIAL AQUI: nas listas em javascript podemos aplicar várias
  funções, porém, abordaremos funções mais a frente no nosso estudo, por enquanto
  basta saber que com o **reduce** nós somaremos todos os valores
  */
  let somaDasNotas = listaDeNotas.reduce((acumulador, notaAtual) => acumulador + notaAtual ,0)

  // aqui descobrimos a media
  let media = somaDasNotas / quantidadeNotas

  // console.log para exibir no terminal as médias dos alunos
  let mensagem = `O aluno ${nome} tirou ${media} de média.`

  console.log(mensagem)
})
