Nas aulas anteriores vimos como configurar o ambiente de programção e comaçamos
a utilizar variáveis, para esta próxima aula o foco é entendermos a criação e
importação de arquivos .js para reutilizarmos códigos, até o momento só utilizamos
os comandos em nosso terminal.
Caso necessário, deixarei os programas criados aqui nas pastas "exemplo-1,
exemplo-2 e assim por diante",basta baixar o código e executar localmente com o
comando "node index.js dentro da pasta do exemplo"

---
Conteúdo da aula:

1. [ Utilização de arquivos .js ](#criacao-de-arquivos)
2. [ Primeiro arquivo .js ](#primeiro-arquivo-js)
3. [ Exemplo 1 ](#exemplo-1)

---

##### 1. Utilização de arquivos .js <a name="criacao-de-arquivos"></a>
- Vamos pensar que você tenha uma lista de alunos e suas notas e quer executar
um programa para calcular e exibir a média de cada aluno. Existem muitos tipos de
arquivos para troca de informação, na programação, um arquivo de texto, um arquivo
XML e até uma planilha de Excel são exemplos de tipos de arquivo para tráfego de
informação.
Como vimos nas aulas anteriores, podemos criar variáveis com listas, para este exemplo, criaremos um arquivo que contenha a nossa lista, para podermos reutilizar esta lista ao longo do nosso programa.

> Para relembrar: Uma lista pode conter números, campos misturados (texto, campos *booleanos*, números ou objetos)
> - Exemplo de uma lista de números: **[1, 2, 3]**
> - Exemplo de uma lista de números: **['um', 'dois', 'três']**
> - Exemplo de uma lista de mista: **[1, 'dois', true]**
> - Exemplo de uma lista de objetos:
>   - **[{ nome: 'Damiano David', dataNascimento: '08/01/1999' },
>   {nome:'Victoria de Angelis', dataNascimento: '28/04/2000'},
>   {nome:'Ethan Torchio', dataNascimento: '08/09/2000'},
>   {nome:'Thomas Raggi', dataNascimento: '18/01/2001'}]**


Agora vamos criar nosso arquivo que utilizaremos para saber a nossa LISTA de nomes e notas, baseado nos exemplos de listas acima, pense qual a melhor opção para armazenarmos os nomes e as notas de nossos alunos.

---
##### 2. Primeiro arquivo .js <a name="primeiro-arquivo-js"></a>

Conseguiu? Isso mesmo, a lista de objetos é a melhor opção neste caso.

pois bem, dentro a pasta exemplo-1 criei o arquivo "alunos.js", a extensão tem que ser exatamente .json, a estrutura do arquivo segue a seguinte notação:

![Imagem 1](img1.png)


Conteúdo da lista:
```javascript
let notas = [
  {
    nome: 'Damiano David',
    notas: [10, 8, 5, 8]
  },
  {
    nome: 'Victoria de Angelis',
    notas: [10, 8, 5, 8]
  }
]
module.exports = notas
```
---
##### 3. Exemplo 1 <a name="exemplo-1"></a>

Certo, agora que criamos nossa primeira lista de alunos e notas a utilizaremos
para criar um programa que calcula as médias de cada aluno.

Para começarmos a utilizar arquivos .js, ao invés de executarmos o node e
utilizá-lo no terminal vamos criar um arquivo com extesão .js, escreveremos nosso
programa e todas as vezes que quisermos calcular as médias, basta executar nosso
arquivo.

Na pasta exemplo-1 criei o arquivo index.js, por convenção, o arquivo
inicial dos programas escritos com js é "index.js", mas isso é só uma convenção,
poderíamos utilizar qualquer nome.

Nosso arquivo index.js será responsável por:
1. Importar nossa lista e disponibilizá-la em uma variável para conseguirmos
trabalhar com os dados
2. Após importar os dados, precisaremos percorrer cada item da nossa lista,
calculando as médias e exibindo o valor no console
  - Observação: até aqui não tem nenhuma novidade, já fizemos exatamente a mesma
  coisa na aula 2, onde utilizamos a função forEach para percorrer uma lista.
  _**Sugiro que você dê uma olhada na aula 2 para buscar a relação entre as duas
  tarefas.**_


###### Mãos a obra
Este é o código no programa, este código está comentado para facilitar o entendimento.

```javascript
// aqui importamos nosso arquivo alunos.js para a variável "alunos"
let alunos = require('./alunos.js')

/* com nossas alunos disponíveis em uma lista chamada "alunos" é hora de percorrermos a lista
calculando as médias:
*/
alunos.forEach((aluno, index) => {
  /* Aqui, cada "aluno" é um item da nossa lista de "alunos" e nós podemos
  acessar o valor de cada propriedade do nosso pela chave
  (no caso, temos as chaves nome e notas)
  */
  let nome = aluno.nome
  let listaDeNotas = aluno.notas

  /* Observação: para calcular a médias precisaremos saber o total das notas e
    dividir isso pela quantidade de notas, para isso, descobriremos QUANTAS NOTAS
    temos utilizando a propriedade "length" da nossa lista
  */

  let quantidadeNotas = listaDeNotas.length

  /* ATENÇÃO ESPECIAL AQUI: nas listas em javascript podemos aplicar várias
  funções, porém, abordaremos funções mais a frente no nosso estudo, por enquanto
  basta saber que com o **reduce** nós somaremos todos os valores
  */
  let somaDasNotas = listaDeNotas.reduce((acumulador, notaAtual) => acumulador + notaAtual ,0)

  // aqui descobrimos a media
  let media = somaDasNotas / quantidadeNotas

  // Mensagem
  let mensagem = `O aluno ${nome} tirou ${media} de média.`

  // console.log para exibir a mensagem no terminal
  console.log(mensagem)
})

```

para executar este código certifique-se de ter os dois arquivos (alunos.js e index.js) na mesma pasta  e execute o script com o comando

`node index.js`

---

#### Finalização
A partir de agora começaremos a tratar mais especificamente de detalhes da programação, a tendência é a dificuldade dos exemplos aumentarem fique à vontade para me procurar no telegram @lucastafarelbs, até a próxima. 
