vamos lá, programação é basicamente separado assim:

1. Programação de scripts:
  Programas que rodam no sistema operacional, serve para subir um programa, instalar um vírus, rodar um comando, já rodou um .bat no windows? é isso!


2. Programação desktop:  
  Programas que rodam offline, direto no computador, um PDV (ponto de venda de mercado), por exemplo

3. Programação WEB:
  Programas que rodam em um servidor e são entregues para o cliente no navegador

  3.1: BACK END - Parte da aplicação que é executada no servidor

  3.2. B: FRONT END - Parte do código que o navegador interpreta

  3.3.  Observação: Um site, um sistema online, uma página de cadastros, tudo isso é programação web.

4. Programação mobile: Programas que rodam nas plataformas móveis


---

Para todas essas vertentes, a base é muito semelhante, precisamos saber que programação se resume a:

1. Lógica e estrutura de dados
2. Paradigmas (orientação a objetos, programação funcional, programação imperativa)
3. Sintaxe da linguagem

  3.1. Cada linguagem é escrita de um jeito, e conta com uma coleção de comandos e convenções para aplicarmos os paradigmas e as lógicas.

portanto, começaremos por aí, para isso utilizaremos javascript que é uma linguagem multiparadigma (pode-se implementar qualquer um dos paradigmas supracitados)

---

> Existem linguagens de alto nível e de baixo nível, dentre elas existe uma subdivisão entre linguagens **interpretadas** e **compiladas**

As linguagens que chamamos de alto nível implementam palavras do cotidiano em inglês, facilitando a compreensão do código, porém, quando o computador que vai processar o programa escrito, ele precisa que esse programa seja "convertido" em linguagem de máquina, para isso linguagens COMPILADAS precisam que o código passe pelo COMPILADOR que gera o programa o em linguagem de máquina que pode então ser executada.
Já as linguagens interpretadas necessitam de que um interpretador esteja sendo executado e então os programas escritos são executados por este interpretador.

O javascript sendo uma linguagem interpretada então, necessita que um interpretador esteja sendo executado para o processador entender o programa.

Para executar nossos programas feitos em javascript no computador usamos a plataforma NODE.JS e para executar no navegador, os navegadores tem o interpretador internamente, o do chrome chama V8.

---
### Parte prática
---
Para programar javascript precisamos de um **EDITOR DE TEXTO**. Sugiro a instalação do **atom**, mas qualquer editor de texto servirá.
- https://atom.io

Também é necessário instalar o node.js na máquina:

- https://nodejs.org/en/

---

Depois de instalado o node, abra o terminal do seu sistema operacional

> no windows, basta ir no iniciar e digitar `cmd`

Execute o node.js com o comando ``node``

após isso o terminal passará a executar o node e tudo o que digitarmos de comando javascript será interpretado, para teste, execute o comando

``console.log('hello world')``

Este comando imprime a mensagem "hello world" no terminal

Com isso temos o ambiente preparado para as próximas aulas.

Até lá.
