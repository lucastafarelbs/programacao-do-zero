Agora que temos o ambiente configurado e conseguimos executar comandos javascript iniciaremos a segunda aula.

---
Conteúdo da aula:

1. [Variáveis ](#variaveis)
2. [Primeiros Comandos](#primeiros-comandos)

---


##### 1. Variáveis <a name="variaveis"></a>

Agora que temos acesso aos comandos javascript no terminal, vamos começar a escrever um programa que exibirá o nome completo de uma pessoa


como vimos na aula anterior, para mostrar mensagens no console temos o comando ``console.log``

então vamos supor que o nome da pessoa seja "Damiano David", queremos escrever a seguinte mensagem: Seja bem vindo, Damiano David.

para isso, você já deve estar se lembrando do console.log, é isso mesmo...

``console.log('Seja bem vindo, Damiano David')``

digite isso e veja a mensagem aparecer no console.

Também queremos mostrar mais mensagens para outras pessoas, porém, imagine que seria extremamente chato ficar trocando manualmente os nomes um por um, para isso, temos as **variáveis**, variáveis são espações que reservamos na memória para armazenar dados.

Imagine que as variáveis são como gavetas, colocamos uma etiqueta nomeando a gaveta e dentro organizamos os dados.

Para criar sua primeira variável, digite no terminal:

> ```let nome = 'Damiano David'```

ótimo, criamos uma "gaveta", capaz de armazenar nomes de pessoas, porém, o que queremos na verdade é exibir uma mensagem. Para isso, digite o seguinte comando:

> ``console.log(`Seja bem vindo ${nome}`)``

OBSERVAÇÃO: não é aspas, aqui o caractere é o acento grave, ou CRASE " \` "

veja o exemplo:
![Exemplo 1](img1.png)

ótimo, agora que temos um espaço na memória para armazenar os nomes, podemos ir trocando os nomes conforme nossa necessidade e sempre executaremos ``console.log(`Seja bem vindo ${nome}`)`` para mostrar a mensagem para a pessoa certa veja o próximo exemplo:

![Exemplo 2](img2.png)

Veja que neste exemplo, só trocamos os nomes (setas azuis), o comando que exibe a mensagem (setas laranjas) se manteve igual em todas as execuções.

Este exemplo parece bobo agora, mas é importante entendermos sempre as necessidades quando estamos programando, neste caso, a necessidade é ter um espaço na memória para armazenar dados para reutilizarmos na execução de nosso programa.

Para adicionar um pouco de complexidade ao nosso exemplo, suponha que precisaremos escrever duas mensagens diferentes, uma para quando for dia e outra para quando for noite.

a mensagem do dia será:

``Bom dia, ${nome}``

e a mensagem da noite será:

``Boa noite, ${nome}``

para não escrever toda vez as mensagens, adicionaremos dois espaços de memória, um para cada tipo de mensagem:

`` let mensagemDia = `Bom dia, ${nome}` ``

`` let mensagemNoite = `Boa noite, ${nome}` ``

Veja como usaremos isso no próximo exemplo:

![Exemplo 3](img3.png)

Neste exemplo temos a **criação de uma variável (1)** para armazenar o nome, depois **criamos as variáveis (2)** para cada uma das mensagens e por fim, **exibimos as mensagens (3)** sem precisar escrever os nomes em cada uma delas.

Assim, encerramos o conteúdo sobre variáveis, aqui só precisamos entender que elas existem e que usaremos muito na programação.


---

##### 2. Primeiros comandos <a name="primeiros-comandos"></a>

todas as linguagens de programação tem uma coleção de comandos que nos permite escrevermos os programas, aqui por exemplo, já vimos o console.log **que imprime mensagens no console**, o let que **cria variáveis**, porém existem muitos outros comandos na sintaxe do javascript para que o programador possa desenvolver as lógicas, começaremos pelos seguintes conteúdos:

no exemplo anterior, criamos os nomes um por vez, isso não é produtivo na prática, seria mais interessante termos uma **lista** de nomes preenchida uma única vez e então imprimirmos as mensagens.

para isso, criaremos uma lista de nomes, com o let:

``let nomes = ['Damiano David', 'Victoria De Angelis', 'Ethan Torchio', 'Thomas Raggi'] ``


> certo, o que queremos é mostrar a menságem "Parabéns, ${nome}!" para cada item da lista, mas e agora para mostrarmos as mensagens para cada um dos nomes?

- O javascript permite o acesso a cada uma das posições de uma lista, por meio do índice, começando por 0, então, o primeiro nome será o índice 0 da lista, assim:

``console.log(`Parabéns, ${nomes[0]}!`)``

Assim o resultado que aparece no nosso console é a mensagem **Parabéns, Damiano David!**, para o comando ``console.log(`Parabéns, ${nomes[1]}!`)`` vemos a mensagem **Parabéns, Victoria De Angelis!** e assim por diante.

> Mas não faz sentido ter uma lista se vamos ter que escrever um comando para cada item dessa lista, certo? "espero que você tenha pensado: "CERTO!". <br>
 Tudo o que queremos com programação é automatizar o máximo de processos possível, nunca faz sentido repetir uma tarefa.

Quando trabalhamos com listas no javascript, temos a possibilidade de percorrer a lista automaticamente, executando um comando a cada item da lista, supondo a lista de nomes do nosso exemplo, com 4 itens, passaremos em cada um desses itens executando um ``console.log`` para imprimir a mensagem para cada nome, o comando que nos permie isso é o **forEach**.

no nosso exemplo, o comando para isso é o seguinte:

`` nomes.forEach(item => console.log(`Parabéns, ${item}!`)) ``

não se apegue ao comando neste momento, falaremos dele e de funções mais a frente em nossos estudos, o importante agora é fixar o conhecimento das variáveis, neste comando cada nome fica armazenado como um **item**, que é uma variável utilizada neste contexto para armazenar cada item da nossa lista de nomes, em uma tradução literal este comando significaria algo como:

`` nomes.forEach: PARA CADA item da lista nome ``

``item  => console.log(`Parabéns, ${item}!`): IMPRIMA NO CONSOLE a mensagem "Parabéns, ${item}!"``

juntando tudo, o comando significaria algo assim: `` tenho uma lista de nomes e quero seja impressa a mensagem Parabéns, nome! no console, sendo que nome é cada um dos itens da lista.``


Veja o exemplo dessa parte:

![Exemplo 4](img4.png)

nessa imagem temos:
1. A **criação** da nossa **lista de nomes**

2. A utilização do comando ``console.log`` acessando CADA item da lista pelo seu índice

3. A utilização da função **forEach**, que nos permite executar um comando **PARA CADA** item da lista, neste caso, **PARA CADA item** executamos o ``console.log`` fazendo uso da variável denominada **item** que armazena cada item da nossa lista, neste exemplo, cada exemplo é um nome na nossa lista de nome.

4. A parte 4 é o resultado do comadno forEach, que nos imprime no console de uma única vez a mesma mensagem substituindo o a parte ${item} por cada item do nossa lista, automatizando nossa vida, de modo que não precisamos preencher manualmente cada nome.

---
#### Finalização

Daqui em diante os conteúdos começarão a ficar mais claros, na prática não usamos tanto o console, para a próxima aula começaremos a ver a criação de arquivos .js e aí a brincadeira começa a ficar divertida.

### telegram

Sugiro que use o telegram e caso tenha alguma dúvida, pode me chamar (**@lucastafarelbs**) , fico à disposição.

Até a próxima.
